const express = require('express');
const app = express();

const driverRoutes = require('./routes/driver.routes');

app.use(express.json());

driverRoutes(app);

module.exports = app;