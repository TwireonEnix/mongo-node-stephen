const driverController = require('../controllers/driver.controller');

module.exports = (app) => {
  app.get('/', driverController.greeting);
  app.post('/', driverController.create);
};