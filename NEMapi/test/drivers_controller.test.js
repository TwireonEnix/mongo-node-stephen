const assert = require('assert');
const request = require('supertest');
const mongoose = require('mongoose');
const app = require('../app');
const Driver = mongoose.model('driver');

describe('Drivers controller', () => {
  it('Post to / create a new driver', done => {
    Driver.count().then(count => {
      request(app).post('/').send({
        email: 'test@test.com'
      }).end(() => {
        Driver.count().then(countAfter => {
          assert(count + 1 === countAfter);
          done();
        })
      })
    });
    /** El send del request funciona para incluir el body */
  });
});