const mongoose = require('mongoose');
const postSchema = require('./post.schema');

/* El objeto schema permite definir un esquema de las entidades. Un blueprint de cómo se verán
los documentos en de la colección, o qué estructura tendrán */
const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name is required'],
    validate: {
      /** Funcion de validación que corre con la propiedad validate, y el mensaje en caso de que
       * falle
       */
      validator: name => name.length > 2,
      message: 'Name must be longer than two characters'
    }
  },
  /** Ya no es necesario el campo de postCount, que se convertirá en un virtual type al poder
   * ser definido a través de otro campo necesario en el esquema.
   */
  // postCount: Number,
  posts: [postSchema],
  likes: Number,
  blogPosts: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'BlogPost'
    }
  ]
});

/** Definición de la propiedad virtual del esquema. Utilizan los getters y setters de ES6,
 * cuando se hace referencia al user.postCount, se utiliza un get, que recibe una función
 * como argumento para hacer el cálculo de cuál será el valor final de esa propiedad vitual
 *
 */
userSchema.virtual('postCount').get(function() {
  /** Al hacer un this, regresa la instancia completa del modelo. IMPORTANTE: El uso de la
   * palabra function() cambia el scope del this. Un this dentro de este function, se refiere
   * al objeto que invoca la función. Si se utilizara una función flecha, el this haría referencia
   * a este módulo.
   */
  return this.posts.length;
});

/** Middleware, se pueden definir funciones middleware en base a hooks en el ciclo de un modelo
 * Este es para hacer un borrado en cascada de los documentos que dependan de la referencia del
 * usuario. Recibe un next para continuar con la ejecución del query. Los hooks disponibles son:
 * validate, save, remove, e init(síncrono).
 */
userSchema.pre('remove', function(next) {
  /** Para evitar la importación cíclica de módulos, se tiene que utilizar el apoyo
   * a través de mongoose para obtener un modelo.
   */
  const BlogPost = mongoose.model('BlogPost');
  BlogPost.deleteMany({ _id: { $in: this.blogPosts } }).then(() => {
    next();
  });
});

/* creación de los modelos que utilizará el schema como blueprint de los documentos de la colección
si no existe la creará. Representa a todos los usuarios de toda la colección de datos en la base. */
module.exports = mongoose.model('User', userSchema);
