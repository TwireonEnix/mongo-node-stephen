const mongoose = require('mongoose');

const blogPostSchema = mongoose.Schema({
  title: String,
  content: String,
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }]
});

module.exports = mongoose.model('BlogPost', blogPostSchema);
