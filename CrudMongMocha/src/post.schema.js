const mongoose = require('mongoose');

/** Para embeber otra entidad dentro de un modelo se crea un nuevo schema, no
 * se crea un modelo, el modelo se hace cuando se quiere crear una colección.
 */
const postSchema = mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Name of post is required']
  }
});

module.exports = postSchema;
