const assert = require('assert');
const User = require('../src/user.model');

describe(' Validating records', () => {
  it('requires a user name', () => {
    const user = new User({ name: undefined });
    /** para hacer la validación síncrona de la instancia del usuario*/
    const validationResult = user.validateSync();
    // console.log(validationResult);
    /** Equivalencia en ES6 para obtener una propiedad con el mismo nombre que el objeto
     * que la va a contener, un destructuring
     */
    // const message = validationResult.errors.name.message;
    const { message } = validationResult.errors.name;

    assert(message === 'Name is required');
  });

  /** Utilizando una función validate dentro del modelo */
  it('user name is at least 2 characters long', () => {
    const user = new User({ name: 'Al' });
    const validationResult = user.validateSync();
    const { message } = validationResult.errors.name;
    assert(message === 'Name must be longer than two characters');
  });

  it('disallows invalid records from being saved', done => {
    const user = new User({ name: 'Al' });
    user
      .save()
      .then(() => {})
      .catch(validationResult => {
        const { message } = validationResult.errors.name;
        assert(message === 'Name must be longer than two characters');
        done();
      });
  });
});
