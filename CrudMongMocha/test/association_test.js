const User = require('../src/user.model');
const BlogPost = require('../src/blog-post.model');
const Comment = require('../src/comment.model');
const assert = require('assert');

describe('Associations', () => {
  let joe, aBlogPost, aComment;
  beforeEach(done => {
    joe = new User({ name: 'Joe' });
    aBlogPost = new BlogPost({
      title: 'JS is great',
      content: 'Some awesome content'
    });
    aComment = new Comment({ content: 'Congrats on great post' });
    /** Creados los documentos se hacen las referencias manualmente,
     * aunque se agregue todo el documento, por el modelo únicamente
     * entra el id.
     */
    joe.blogPosts.push(aBlogPost);
    aBlogPost.comments.push(aComment);
    aComment.author = joe;

    // joe.save();
    // aBlogPosts.save();
    // aComment.save();

    /** Implementación ES6 para que todas las promesas anteriores se
     * condensen en una sola promesa para llamar al done únicamente
     * cuando todas hayan resuelto
     */
    Promise.all([joe.save(), aBlogPost.save(), aComment.save()]).then(() =>
      done()
    );
  });

  /** Cuando se tienen demasiados tests para ejecutar, el it.only hará
   * la prueba únicamente aquí.
   */
  it('saves a relation between a user and a blog post', done => {
    User.findOne({ name: 'Joe' })
      /** populate necesita el parámetro del campo donde se quiere cargar un documento
       * de otra clase
       */
      .populate('blogPosts')
      .then(user => {
        /** Al ver el objeto de blogPosts, éste tiene otra referencia dentro
         * que mongoose ya no carga por razones de performance.
         */
        // console.log(user.blogPosts[0]);
        assert(user.blogPosts[0].title === 'JS is great');
        done();
      });
  });

  /** Populate nested */
  it('saves a full relation graph', done => {
    /** Populate también puede recibir un objeto con configuración con ciertas propiedades
     * path: el campo que se quiere popular,
     * populate: Dentro del path principales, cargar asociaciones extras.
     * Esto se puede ciclar hasta llegar al nivel de referencia más bajo a expensas
     * del performance de la aplicación. Es necesario indicar además, el modelo del
     * que proviene dicha referencia.
     */
    User.findOne({ name: 'Joe' })
      .populate({
        path: 'blogPosts',
        populate: {
          path: 'comments',
          model: 'Comment',
          populate: { path: 'author', model: 'User' }
        }
      })
      .then(user => {
        // console.log(user.blogPosts[0].comments[0]);

        /** Assert iniciales */
        // assert(user.name === 'Joe');
        // assert(user.blogPosts[0].title === 'JS is great');
        // assert(
        //   user.blogPosts[0].comments[0].content === 'Congrats on great post'
        // );
        // assert(user.blogPosts[0].comments[0].author.name === 'Joe');

        /** Práctica destructuring  */
        const { name, blogPosts } = user;
        const { comments } = blogPosts[0];
        const { author } = comments[0];
        assert(name == 'Joe');
        assert(blogPosts[0].title === 'JS is great');
        assert(comments[0].content === 'Congrats on great post');
        assert(author.name === 'Joe');
        done();
      });
  });
});
