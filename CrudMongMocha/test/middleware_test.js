const assert = require('assert');
const User = require('../src/user.model');
const BlogPost = require('../src/blog-post.model');
const bcrypt = require('bcryptjs');

describe('Middleware to delete', () => {
  let joe, aBlogPost;
  beforeEach(done => {
    joe = new User({
      name: 'Joe'
    });
    aBlogPost = new BlogPost({
      title: 'JS is great',
      content: 'Some awesome content'
    });
    joe.blogPosts.push(aBlogPost);
    Promise.all([joe.save(), aBlogPost.save()]).then(() => done());
  });

  it('Users clean up dangling blogposts on delete', done => {
    joe
      .delete()
      .then(() => {
        /** El modelo .count (deprecated, usar countDocuments instead) regresa el número de documentos
         * que haya en la colección */
        return BlogPost.countDocuments();
      })
      .then(count => {
        assert(count === 0);
        done();
      });
  });

  it.only('Generate a hash', done => {
    bcrypt.genSalt(10).then(salt => {
      return bcrypt.hash('somePassword', salt);
    }).then(hash => {
      console.log(hash);
      done();
    });
  });
});