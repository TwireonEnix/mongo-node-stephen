const assert = require('assert');
const User = require('../src/user.model');

describe('Reading users out of the database', () => {
  let joe, maria, alex, hal, zach;

  /**el done le indica a mocha que el test llevará un tiempo indefinido de tiempo, una vez completa
   * la promesa, continuará con el test siguiente.
   */
  beforeEach(done => {
    /* Una vez que se crea la instancia del modelo, mongoose automáticamte le asingna un objectId, 
    que funcionará para hacer el test, corroborando que la lectura fue del mismo objeto creado
    previamente. Se crearán varios para usar en paginación */
    joe = new User({ name: 'Joe' });
    maria = new User({ name: 'Maria' });
    alex = new User({ name: 'Alex' });
    hal = new User({ name: 'Hal' });
    zach = new User({ name: 'Zack' });
    /** Como nota adicional, Promise.all regresa los resultados en un arreglo al hacer console.log del resultado */
    Promise.all([
      joe.save(),
      maria.save(),
      alex.save(),
      hal.save(),
      zach.save()
    ]).then(() => {
      done();
    });
  });

  it('Finds all users with a name of joe', done => {
    /* Find encuentra todos los docs que coincidan con el criterio, regresa un arreglo.
    FindOne regresa únicamente un documento que cumpla el criterio  */
    User.find({ name: 'Joe' }).then(users => {
      /* usando el id que genera automáticamente para verificar que el record de la base de datos sea
      el mismo insertado previamente. Al inicio el assert falla porque el objectId que les asigna el
      odm es distinto, aunque encierren el mismo id. Se necesita hacer un parseo para comparar */
      // console.log(users[0]._id);
      // console.log(joe._id);
      assert(users[0]._id.toString() === joe._id.toString());
      done();
    });
  });

  it('Finds one user with the name of Joe using findOne', done => {
    User.findOne({ _id: joe._id }).then(user => {
      assert(user.name === 'Joe');
      done();
    });
  });

  it('Finds one user by id', done => {
    User.findById(joe._id).then(user => {
      assert(user.name === 'Joe');
      done();
    });
  });

  /** Filtrar para hacer paginación */
  it('can skip and limit the result set', done => {
    /** Los records se traen en el orden en el que fueron ingresados a la bd,
     * Joe -> Maria -> Alex -> Hal -> Zach
     * entonces skip(1).limit(2) solo debería traer a Maria y Alex.
     * Sin embargo, por ser operaciones asíncronas que toman tiempo en ejecutarse, el orden en que termina
     * la inserción en la bd no siempre será en el mismo orden, por lo tanto lo anterior no siempre funciona.
     */
    User.find()
      /** Sort recibe un objeto con el campo que se quiere ordenar, y un valor 1 para ascendente, -1 para
       * descendente
       */
      .sort({ name: 1 })
      .skip(1)
      .limit(2)
      .then(users => {
        assert(users.length === 2);
        assert(users[0].name === 'Hal');
        assert(users[1].name === 'Joe');
        done();
      });
  });
});
