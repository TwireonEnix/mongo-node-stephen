const mongoose = require('mongoose');
const conf = require('dotenv').config();

if (conf.error) throw conf.error;

/* Para que mongoose utilice la implementación de promesas por default en ES6 */
mongoose.Promise = global.Promise;
/** Para omitir las funciones obsoletas que mongoose aún no pone al corriente con el
 * driver de mongo "Deprecation warnings"
 */
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

mongoose.connect(
  'mongodb+srv://' +
    process.env.COURSE_DB_USER +
    ':' +
    process.env.COURSE_DB_PASS +
    '@' +
    process.env.COURSE_DB_HOST +
    '/StephenMongo?retryWrites=true01',
  {
    useNewUrlParser: true
  }
);

/* sección para montar un servidor http con la conexión */
// .then(() => {
//   const port = process.env.PORT;
//   app.set('port', port);
//   http.createServer(app).listen(port, () => {
//     console.log(`DB online and server started on ${port}`);
//   });
// }).catch(error => console.log('Failure') + error);

/* Otro hook del test framework de mocha que se ejecuta antes de las pruebas, la diferencia entre este y
before each es que solo se corre una vez. Esta se hará para testear la conexión a la bd */
before(done => {
  mongoose.connection
    .once('open', () => {
      console.log('Good to go');
      done();
    })
    .on('error', error => {
      console.warn('Warning', error);
    });
});

/* Open y error son eventos de la librería de mongoose, funciona para abrir la conexión creada
en el método connect */

/* Hooks después de que el código de callback dentro de drop llama el done, ya es aceptado 
  segui con los siguientes tests */

beforeEach(done => {
  /* remueve toda la colección usuarios antes de las pruebas*/
  const { users, comments, blogposts } = mongoose.connection.collections;
  users.drop(() => {
    comments.drop(() => {
      blogposts.drop(() => {
        done();
      });
    });
  });
});
