const assert = require('assert');
const User = require('../src/user.model');

/** Del lado de la instancia: update y 'set' and 'save */

describe('Updating user via instance', () => {
  let joe;

  beforeEach(done => {
    joe = new User({ name: 'Joe', likes: 0 });
    joe.save().then(() => {
      done();
    });
  });

  /** Se utilizará una función para ser llamada desde los its y evitar repetir
   * el código de comprobar la actualización del record.
   */
  function assertName(operation, done) {
    operation
      .then(() => {
        return User.find();
      })
      .then(users => {
        assert(users.length === 1);
        assert(users[0].name === 'Alex');
        done();
      });
  }

  it('instance type using set n save', done => {
    /** Al hacer un set, el cambio solo está reflejado en memoria, para persistirlo
     * en la bd se tiene que guardar el documento con el método save
     */
    joe.set('name', 'Alex');
    /** Para hacer el assert, el test tiene que asegurar que no existan records con el
     * nombre Joe y que exista uno con el nombre Alex
     */
    assertName(joe.save(), done);
  });

  it('instance type can update', done => {
    /** En este approach todo lo guarda de una vez, en contrario con el set and save que es
     * posible utilizar el objeto varias veces antes de salvarlo
     */
    assertName(joe.updateOne({ name: 'Alex' }), done);
  });

  it('A model class can update', done => {
    /** Recibe dos parámetros, lo que hay que buscar y cómo se va a modificar */
    assertName(User.update({ name: 'Joe' }, { name: 'Alex' }), done);
  });

  it('A model class can update one record.', done => {
    assertName(User.findOneAndUpdate({ name: 'Joe' }, { name: 'Alex' }), done);
  });

  it('A model class can get a record given specific id and update', done => {
    assertName(User.findByIdAndUpdate(joe._id, { name: 'Alex' }), done);
  });

  /** Al usar una x en frente del it se pone el test como pendiente y no se cuenta en
   * las pruebas. Se puso pendiente para arreglar la eliminación del campo de postCount
   */
  it('Take a user and increment post count by one', done => {
    /** Cada vez que se evite obtener información de mongo se genera una ganancia en performance, entonces
     * el uso de operadores de actualización es un método eficiente para hacer actualizaciones a los
     * documentos sin cargar el documento en la memoria del servidor.
     */
    User.update({ name: 'Joe' }, { $inc: { likes: 1 } })
      .then(() => {
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        assert(user.likes === 1);
        done();
      });
  });
});
