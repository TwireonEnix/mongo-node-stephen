const User = require('../src/user.model');
const assert = require('assert');

describe('Deleting a user', () => {
  let joe;
  /** Mongoose utiliza la terminología de remove, hay 4 maneras de remover datos de la bd,
   * remove, findOneAndRemove, findByIdAndRemove, de la clase del modelo y con la instancia
   * del modelo:
   * NOTA: Remove es obsoleto, usar delete instead
   */

  beforeEach(done => {
    joe = new User({ name: 'Joe ' });
    joe.save().then(() => done());
  });

  it('Model Instance remove', done => {
    /** Remueve la instancia del model una vez recuperado de la bd */
    joe
      .delete()
      .then(() => {
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        assert(user === null);
        done();
      });
  });

  it('Class method remove', done => {
    /** Remueve todos los documentos de la colección que cumplan con la
     * criteria
     */
    User.deleteOne({ name: 'Joe' })
      .then(() => {
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        assert(user === null);
        done();
      });
  });

  it('Class method findAndRemove', done => {
    User.findOneAndDelete({ name: 'Joe' })
      .then(() => {
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        assert(user === null);
        done();
      });
  });

  it('Class method findById and Remove', done => {
    User.findByIdAndDelete(joe._id)
      .then(() => {
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        assert(user === null);
        done();
      });
  });
});
