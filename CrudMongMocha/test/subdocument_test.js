const assert = require('assert');
const User = require('../src/user.model');

describe('Subdocuments', () => {
  it('can create a subdocument', done => {
    const joe = new User({
      name: 'Joe',
      posts: [{ title: 'An awesome title' }]
    });
    /** cuando se tiene un documento embebido, en el ejemplo se salva de la misma manera
     * que siempre
     */
    joe
      .save()
      .then(() => {
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        assert(user.posts.length > 0);
        assert(user.posts[0].title === 'An awesome title');
        done();
      });
  });

  /** No es posible guardar los subdocumentos por sí solos, debe existir el
   * documento padre para poder existir
   */
  it('can add subdocuments to an existing record', done => {
    /** Flujo: 1. Crear Joe sin posts */
    const joe = new User({
      name: 'Joe',
      /** No es necesario inicializar arreglos aquí, pero por demostración se hará */
      posts: []
    });
    /** 2. Salvar joe */
    joe
      .save()
      .then(() => {
        /** Retorno de promesa: 3. Traer a joe de la bd */
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        /** Obtener el documento, modificarlo en memoria y salvarlo de nuevo a mongo.
         * 4. Agregar un nuevo post a la propiedad posts
         */
        user.posts.push({ title: 'An awesome new post' });
        /** Persistencia en db del objeto modificado */
        return user.save();
      })
      .then(() => {
        /** Volver a traer el documento de la bd */
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        /** Hacer la aserción de que hay un post con el título propuesto */
        assert(user.posts[0].title === 'An awesome new post');
        done();
      });
  });

  it('should delete an existing subdocument', done => {
    const joe = new User({
      name: 'Joe',
      posts: [{ title: 'New Title' }]
    });
    joe
      .save()
      .then(() => {
        /** Para no ocupar los métodos clásicos de los arreglos para eliminar un elemento,
         * se utilizará lo que provee mongoose, delete (remove en el video pero esta deprecated)
         * delete se ocupa en el objeto que se va a remover, no quita el documento padre. La
         * diferencia aquí es que cuando se remueve un subdocumento el documento padre no se salva
         * automáticamente, hay que salvar el objeto manualmente.
         */
        return User.findOneAndUpdate(
          { name: 'Joe' },
          { $pull: { posts: { title: 'New Title' } } }
        );
      })
      .then(() => {
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        assert(user.posts.length === 0);
        done();
      });
  });
});
