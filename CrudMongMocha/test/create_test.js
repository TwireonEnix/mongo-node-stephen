/* Testing del bloque de creación de usuarios definido en src */

/* paquete que sirve para hacer aserciones dentro de los bloques de código it a testear */
const assert = require('assert');
const User = require('../src/user.model');
/* Bloques describe para hacer tests, recibe dos parémtros, un string con el nombre del test
 y el segundo es una función que contiene todos los bloques de it, que se deben testear */

describe('Creating records', () => {
  /* bloque it que recibe dos parámetros, el nombre y la función que se debe probar. Dentro de it
  se debe hacer una aserción, para escribir el valor que se espera recibir de la prueba para detectar
  que el bloque de las funciones se resuelven correctamente. 
  
  Al llevar una promesa dentro del código de it, se tiene que hacer referencia al método done para poder
  pasar el control a la siguiente prueba esperando la resolución de la promesa*/
  it('saves a user', (done) => {
    /* Crea una nueva instancia del modelo User definido previamente */
    const joe = new User({
      name: 'Joe'
    });
    joe.save().then(() => {
      /* Ha sido Joe ingresado en la bd? Cuando una instancia de un esquema de mongoose se crea obtiene
      una bandera de isNew en true, cuando entra a la base de datos exitosamente, esa bandera se 
      niega y es como se puede hacer la aserción, para que una aserción pase, debe recibir un booleano */
      assert(!joe.isNew);
      done();
    });
  });
});