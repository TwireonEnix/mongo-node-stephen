const assert = require('assert');
const User = require('../src/user.model');

describe('Virtual types', () => {
  it('postCount returns number of posts', done => {
    const joe = new User({
      name: 'Joe',
      posts: [{ title: 'A new title' }]
    });
    joe
      .save()
      .then(() => {
        return User.findOne({ name: 'Joe' });
      })
      .then(user => {
        assert(user.postCount === 1);
        done();
      });
  });
});
