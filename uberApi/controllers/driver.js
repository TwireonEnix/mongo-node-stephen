const { Driver } = require('../models/driver');

module.exports = {

  greeting(req, res, next) {
    res.send({ hi: 'there' });
  },

  createDriver(req, res, next) {
    const driverProps = { email: req.body.email };
    Driver.create(driverProps).then(driver => {
      res.send(driver);
    }).catch(next);
  },

  editDriver(req, res, next) {
    const driverProps = { email: req.body.email, driving: req.body.driving };
    Driver.findOneAndUpdate({ _id: req.params.id }, { $set: driverProps }, { new: true })
      .then(modifiedDriver => {
        if (!modifiedDriver) return res.status(404).send('Driver not found');
        res.send(modifiedDriver);
      }).catch(next);
  },

  deleteDriver(req, res, next) {
    Driver.findByIdAndRemove({ _id: req.params.id }).then(deletedDriver => {
      if (!deletedDriver) return res.status(404).send('Driver not found');
      res.status(200).send(deletedDriver);
    }).catch(next);
  }

}