const config = require('dotenv').config();
if (config.error) throw new Error('Fatal Error: Necessary .env variables not found');
const express = require('express');
const app = express();

require('./helpers/db')();
require('./helpers/joi-conf')();
require('./helpers/routes')(app);

module.exports = app;