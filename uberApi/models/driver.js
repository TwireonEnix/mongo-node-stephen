const mongoose = require('mongoose');
const Joi = require('joi');

const driverSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  driving: {
    type: Boolean,
    default: false
  }
});

const Driver = mongoose.model('driver', driverSchema);

const validateDriver = driver => Joi.validate(driver, {
  email: Joi.string().email({ minDomainAtoms: 2 }).min(5).max(255).required(),
  driving: Joi.boolean()
});


module.exports.Driver = Driver;
module.exports.validate = validateDriver;