const express = require('express');
const router = express.Router();
const driverCtrl = require('../controllers/driver');
const { validate } = require('../models/driver');
const validator = require('../middleware/validator');
const validId = require('../middleware/validId');

router.get('/', driverCtrl.greeting);
router.post('/', validator(validate), driverCtrl.createDriver);
router.put('/:id', [validId, validator(validate)], driverCtrl.editDriver);
router.delete('/:id', validId, driverCtrl.deleteDriver);

module.exports = router;