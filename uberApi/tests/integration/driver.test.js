const { expect } = require('chai');
const request = require('supertest');
const mongoose = require('mongoose');
const Driver = mongoose.model('driver');
const { ObjectId } = mongoose.Types;
const app = require('../../app');

describe('POST /api/driver', () => {
  let payload;
  const execute = () => request(app).post('/api/driver').send(payload);

  beforeEach(done => {
    payload = { email: '12345@example.com' };
    done();
  });

  afterEach(done => {
    Driver.deleteMany().then(() => done());
  });

  it('should return 400 if no email is provided', done => {
    delete payload.email;
    execute().expect(400).end(done);
  });

  it('should create a new driver in db', done => {
    Driver.countDocuments().then(count => {
      execute().expect(200).end((err, res) => {
        Driver.countDocuments()
          .then(newCount => {
            expect(newCount).to.equal(count + 1);
            return Driver.findOne(payload);
          })
          .then(driver => {
            expect(driver).to.exist;
            expect(driver.email).to.equal(payload.email);
            done();
          });
      });
    });
  });

  it('should send a 500 if email is already taken', done => {
    Driver.create(payload).then(() => {
      execute().expect(500).end(done);
    });
  });

  it('should send 400 if email is not valid', done => {
    payload.email = '12345';
    execute().expect(400).end(done);
  });
});

describe('PUT /api/driver', () => {
  let payload, driver, driverId;

  const execute = () => request(app).put(`/api/driver/${driverId}`).send(payload);

  beforeEach(done => {
    driverId = new ObjectId();
    driver = { email: '12345@gmail.com', _id: driverId };
    payload = { email: 'abc123@gmail.com' };
    Driver.create(driver).then(() => done());
  });

  afterEach(done => {
    Driver.deleteMany().then(() => done());
  });

  it('should return 400 if id is invalid', done => {
    driverId = null;
    execute().expect(400).end(done);
  });

  it('should return 400 if no email is provided', done => {
    delete payload.email;
    execute().expect(400).end(done);
  });

  it('should return 404 if there is no driver with the valid id passed', done => {
    driverId = new ObjectId();
    execute().expect(404).end(done);
  });

  it('should update the driver', done => {
    payload.driving = true;
    execute().expect(200).end((err, res) => {
      expect(res.body.email).to.equal(payload.email);
      Driver.findById(driverId).then(driverInDb => {
        expect(driverInDb.email).to.equal(payload.email);
        expect(driverInDb.driving).to.be.true;
        done();
      });
    });
  });
});

describe('DELETE /api/driver', () => {
  let driverId, payload;

  const execute = () => request(app).delete(`/api/driver/${driverId}`);

  beforeEach(done => {
    driverId = new ObjectId();
    payload = { _id: driverId, email: '12345@example.com' };
    Driver.create(payload).then(() => done());
  });

  afterEach(done => {
    Driver.deleteMany({}).then(() => done());
  });

  it('should return 400 if provided id is not valid or not present', done => {
    driverId = null;
    execute().expect(400).end(done);
  });

  it('should return 404 if no driver with provided id', done => {
    driverId = new ObjectId();
    execute().expect(404).end(done);
  });

  it('should return 200 and return the deleted driver', done => {
    execute().expect(200).end((err, res) => {
      expect(res.body.email).to.equal(payload.email);
      expect(res.body.driving).to.be.false;
      Driver.countDocuments().then(count => {
        expect(count).to.equal(0);
        done();
      });
    });
  });
});