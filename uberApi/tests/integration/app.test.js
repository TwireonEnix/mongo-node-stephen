const { expect } = require('chai');
const request = require('supertest');
const app = require('../../app');

describe('The express app', () => {
  it('should return a greeting with get api', done => {
    request(app).get('/api/driver')
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.eql({ hi: 'there' });
        done();
      });
  });
});