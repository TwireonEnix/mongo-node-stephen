const express = require('express');
const driver = require('../routes/driver')
const error = require('../middleware/error');

module.exports = app => {
  app.use(express.json());
  if (app.get('env') === 'development') app.use(require('morgan')('dev'));
  app.use('/api/driver', driver);
  app.use(error)
}