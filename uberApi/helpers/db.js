const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

module.exports = () => {
  let db = 'muber'
  if (process.env.NODE_ENV === 'test') db = 'muber_tests';
  const connectionString =
    `mongodb+srv://${process.env.ATLAS_USER}:${process.env.ATLAS_PASS}@${process.env.ATLAS_HOST}/${db}?retryWrites=true01`;
  mongoose.connect(connectionString, { useNewUrlParser: true })
    .then(() => {})
    .catch(err => { throw new Error(err); });
}